/**
 * @description
 * webpack development configs
 */
const Common = require('./config/webpack/commons');
const Rules = require('./config/webpack/rules');

/**
 * @description
 * node.js core handles the path
 * */
const path = require('path');
const resolvePath = (folder) => { return path.resolve(__dirname, folder) };

const OUTPUT = { path: resolvePath('dist'), ...Common.output};

module.exports = {
    output: OUTPUT,
    module: { rules: Rules },
    resolve: {
        modules: ['node_modules'],
        alias: {
            public: resolvePath('public')
        },
        extensions: ['.js', '.json', 'scss', 'css', '.min.js', '.min.css']
    },
    optimization: {
        minimize: true,
        runtimeChunk: true,
        splitChunks: {
            chunks: 'async',
            cacheGroups: {
                default: false,
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                    reuseExistingChunk: true
                }
            }
        }
    }
};