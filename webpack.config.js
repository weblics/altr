/**
 * @description
 * webpack configs
 */
const development = require('./webpack.dev');
const production = require('./webpack.common');

module.exports = (env, args) => {
    if(args.mode === 'production') {
        return production
    } else {
        return development
    }
};