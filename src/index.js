import './index.scss';

import 'jquery';
import 'slick-carousel';

$(document).ready(() => {
  $('.js-carousel').slick({
    slidesToShow: 3,
    centerMode: true,
    prevArrow: "<div class='work__nav work__nav--prev'></div>",
    nextArrow: "<div class='work__nav work__nav--next'></div>",
    responsive: [
      {
        breakpoint: 840,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
});
