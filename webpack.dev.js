/**
 * @description
 * webpack development configs
 */
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;

const merge = require('webpack-merge');
const Common = require('./config/webpack/commons');
const commonConfig = require('./webpack.common');

const PLUGINS = [
    ...Common.plugins,
    new BundleAnalyzerPlugin(),
];

module.exports = merge(commonConfig, {
    devtool: 'eval-source-map',
    watch: true,
    devServer: {
        inline: true
    },
    devtool: '',
    stats: {
        colors: true
    },
    plugins: PLUGINS
});