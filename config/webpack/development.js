/**
 * @description
 * webpack development configs
 */
const HtmlPlugin = require('./commons');
const Rules = require('./rules');

/**
 * PLUGINS PLACE
 * @type {webpack|exports|module.exports}
 */
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;

/**
 * @description
 * node.js core handles the path
 * */
const files = [ 'index' ];
const resolvePath = (folder) => { return PATH.resolve(__dirname, folder) };
const htmlPlugins = HtmlPlugin.register(files);

const PATH = require('path');
const OUTPUT = {
    path: resolvePath('dist'),
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js',
    publicPath: '/dist'
};
const PLUGINS = [
    new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
        jQuery: 'jquery',
        'window.jquery': 'jquery'
    }),
    new webpack.EnvironmentPlugin(['NODE_ENV', 'DEBUG']),
    new BundleAnalyzerPlugin(),
    ...htmlPlugins
];

module.exports = {
    devtool: 'eval-source-map',
    watch: true,
    devServer: {
        inline: true
    },
    devtool: '',
    stats: {
        colors: true
    },
    output: OUTPUT,
    module: { rules: Rules },
    resolve: {
        modules: ['node_modules'],
        alias: {
            public: resolvePath('public')
        },
        extensions: ['.js', '.json', 'scss', 'css', '.min.js', '.min.css']
    },
    plugins: PLUGINS,
    optimization: {
        minimize: true,
        runtimeChunk: true,
        splitChunks: {
            chunks: 'async',
            cacheGroups: {
                default: false,
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                    reuseExistingChunk: true
                }
            }
        }
    }
};