const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

function _registerHtmlPlugin(file) {
    return new HtmlWebPackPlugin({
        template: `./src/${file}.html`,
        filename: `./${file}.html`
    });
};

function _register(files) { return files.map(file => _registerHtmlPlugin(file)); };

const files = [ 'index' ];
const htmlPlugins = _register(files);

module.exports.output = {
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js',
    publicPath: '/dist'
};

module.exports.plugins = [
    new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
        jQuery: 'jquery',
        'window.jquery': 'jquery'
    }),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new CleanWebpackPlugin('dist'),
    ...htmlPlugins
];

