/**
 * RULES PLACES
 * 
 * @return array
 * */
const general = [
    {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader'
        }
    },
    
    {
        test: /\.scss$/,
        use: [
        {loader: "style-loader"},
        {   
            loader: "css-loader",
            options: {
                minimize: true
            }
        },
        {
            loader: 'sass-loader',
            options: {
                minimize: true
            }
        }]
    },

    {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
                mozjpeg: {
                    progressive: true,
                    quality: 65
                  },
                  optipng: {
                    enabled: false,
                  },
                  pngquant: {
                    quality: '65-90',
                    speed: 4
                  },
                  gifsicle: {
                    interlaced: false,
                  },
                  webp: {
                    quality: 75
                  }
            },
          },
        ]
    }
];

 const withHtml = [
    ...general,
    {
        test: /\.html$/,
        use: [{
            loader: 'html-loader',
            options: {
                minimize: true
            }
        }]
    }
 ];

module.exports = general;